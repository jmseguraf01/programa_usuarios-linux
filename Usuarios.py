#!/usr/bin/env python
#-*- coding: utf-8-*-
#enconding: utf-8


# _________________________________________________________
#|						                                   |
#|		CREADO POR JUAN MIGUEL SEGURA FERNANDEZ.           |
#|		CORREO: jusefer2@hotmail.es                        |
#|_________________________________________________________|

#----- FICHEROS ----------------------------------------------------------------------------------------
#  - Users.txt = contiene los usuarios de inicio de sesion
#  - Error_user.txt = contiene los intentos fallidos de inicio de sesion
#  - License.txt = contiene la licencia del programa. Licencia GNU
#  - Ultimo_inicio.txt = tiene registrados los ultimos inicios de sesion
#  - Correo.txt = contiene la direccion de correo del usuario para enviar un correo al iniciar sesion
# -------------------------------------------------------------------------------------------------------

def validar():
	import commands
	os.system("clear")

        os.system("banner Welcome | lolcat")

    # PARA SABER SI ES LA PRIMERA VEZ QUE SE INICIA EL PROGRAMA
	primeravez=commands.getoutput("ls")

	if "primeravez.txt" in primeravez:
		primeravez=True
	else:
		primeravez=False


	# El programa se ejecuta por primera vez
	if primeravez==True:
		os.system("clear")

		#----- COMPROBACION DE QUE QUE GPG ESTA INSTALADO ----
		print "Comrpobando que esten los paquetes necesarios para que el programa funcione..."
		version=commands.getoutput("lsb_release -a")
		print ""

		#SI ES LA VERSION 18.04 HAY QUE INSTALAR GPG1, NO GPG
		if "18" in version:
                        print "Version de Ubuntu: 18.04"
			gpg1=commands.getoutput("which gpg1")

			#SI TIENE INSTALADO GPG1
			if "gpg1" in gpg1:
				print "Tienes instalado el cifrado de la Base de datos.\nPodemos empezar la insttalacion."
				time.sleep(2.5)
				os.system("clear")

			else:
				print "No tienes gpg1 instalado."
				install_gpg1=raw_input("Deseas instalar gpg1? (S/N)")

				if install_gpg1=="S" or install_gpg1=="s":
					print "Instalando gpg1..."
					os.system("sudo apt install gpg1")

				else:
					print "Si no se instala gpg1 no se puede ejecutar el programa..."
					time.sleep(2)
					exit()

		#SI NO ES LA VERSION 18.04 NO HAY QUE INSTALAR GPG1, PERO TIENE QUE TENER INSTALADO GPG
                else:
                    print "Version difeerente a Ubuntu 18.04"
                    gpg=commands.getoutput("which gpg")

                    #SI TIENE INSTALADO GPG
                    if "gpg" in gpg:
                        print "Tienes instalado el cifrado de la Base de datos.\nPodemos empezar la insttalacion."
                        time.sleep(2)
                        os.system("clear")

                    #SI NO TIENE INSTALADO GPG
                    else:
                        print "No tienes gpg instalado"
                        install_gpg=raw_input("Deseas instalar gpg? (S/N): ")

                        if install_gpg=="s" or install_gpg=="S":
                            print "Instalado cifrado gpg"
                            os.system("sudo apt install gpg")

                        else:
                            print "Si no se instala gpg no se puede ejecutar el programa..."
                            time.sleep(2)
                            os.system("clear")
                            exit()




		print "Esta es la primera vez que utilizas el programa!\nPara poder acceder a tus contraseñas necesitas crear un usuario como minimo.\nPodras crear hasta 3 usuarios y modificarlos en cualquier momento escribiendo 'help' en el menu.\n"
		usuario_primeravez=raw_input("Que nombre deseas ponerle al usuario? ")

		#ESTABLECE CONEXION CON EL TXT CON LOS USUARIOS
		f=open("Users.txt","r")

		l1=f.next()
		usuario0=f.next()
		usuario1=f.next()
		usuario2=f.next()

		#LE QUITO LA -
		usuario0=usuario0.replace("-","")
		usuario1=usuario1.replace("-","")
		usuario2=usuario2.replace("-","")

		#LE QUITO EL SALTO DE LINEA (\n) PARA QUE NO ME DE ERROR DE QUE NO ES IGUAL AL usuario_validar
		usuario0=usuario0.replace("\n","")
		usuario1=usuario1.replace("\n","")
		usuario2=usuario2.replace("\n","")

		f2=open("Users.txt", "w")

		#HAGO QUE ESCRIBA TODOS LOS DATOS CAMBIANDO SOLO EL USUARIO. EL \n ES PARA QUE SALTE LA LINEA
		f2.write("%s%s\n%s\n%s\n" %(l1,usuario_primeravez, usuario1, usuario2))
		os.system("clear")

		#Creo las carpetas de los usuarios, copio la base de datos y le cambio el nombre
		os.system("mkdir %s" %(usuario_primeravez))
		os.system("cp Datos_Usuarios_vacio.s3db %s/"%(usuario_primeravez))
		os.system("mv %s/Datos_Usuarios_vacio.s3db %s/Datos_Usuarios.s3db"%(usuario_primeravez, usuario_primeravez))
		print "Escoge contraseña para el usuario: %s" %(usuario_primeravez)
		commands.getoutput("gpg -c %s/Datos_Usuarios.s3db" %(usuario_primeravez))
		commands.getoutput("rm %s/Datos_Usuarios.s3db" %(usuario_primeravez))

		os.system("clear")
		os.system("mkdir %s" %(usuario1))
		os.system("cp Datos_Usuarios_vacio.s3db %s/"%(usuario1))
		os.system("mv %s/Datos_Usuarios_vacio.s3db %s/Datos_Usuarios.s3db"%(usuario1, usuario1))
		print "Escoge contraseña para el usuario: %s" %(usuario1)
		commands.getoutput("gpg -c %s/Datos_Usuarios.s3db"%(usuario1))
		commands.getoutput("rm %s/Datos_Usuarios.s3db" %(usuario1))


		os.system("clear")
		os.system("mkdir %s" %(usuario2))
		os.system("cp Datos_Usuarios_vacio.s3db %s/"%(usuario2))
		os.system("mv %s/Datos_Usuarios_vacio.s3db %s/Datos_Usuarios.s3db"%(usuario2, usuario2))
		print "Escoge contraseña para el usuario: %s" %(usuario2)
		commands.getoutput("gpg -c %s/Datos_Usuarios.s3db" %(usuario2))
		commands.getoutput("rm %s/Datos_Usuarios.s3db" %(usuario2))





		print "El usuario ha sido creado con exito."
		f2.close()
		time.sleep(1)

		#BORRO EL PRIMERAVEZ.TXT Y LE PONGO FALSE PARA QUE VAYA A LA VALIDACION
		commands.getoutput("rm primeravez.txt")
		primeravez=False

	# El programa no se ejecuta por primera vez
	if primeravez==False:
		#ESTABLECE CONEXION CON EL TXT CON LOS USUARIOS
		f=open("Users.txt","r")

		l1=f.next()
		usuario0=f.next()
		usuario1=f.next()
		usuario2=f.next()

		#LE QUITO LA -
		usuario0=usuario0.replace("-","")
		usuario1=usuario1.replace("-","")
		usuario2=usuario2.replace("-","")

		#LE QUITO EL SALTO DE LINEA (\n) PARA QUE NO ME DE ERROR DE QUE NO ES IGUAL AL usuario_validar
		usuario0=usuario0.replace("\n","")
		usuario1=usuario1.replace("\n","")
		usuario2=usuario2.replace("\n","")


		usuario_validar=raw_input("Introduzca el nombre de usuario: ")
		correcto=False

		if usuario_validar=="s":
			os.system("clear")
			f.close()
			exit()

		#Defino la variable de si han acertado el nombre de usuario
		usuario_correcto=False

		#Si aciertan el nombre de usuario la variable se pone True
		if usuario_validar==usuario0:
			usuario_correcto=True

		if usuario_validar==usuario1:
			usuario_correcto=True

		if usuario_validar==usuario2:
			usuario_correcto=True

		#Si el usuario es correcto
		if usuario_correcto==True:
			# Descifro la base de datos en la carpeta del usuario
			contrasena_principal=getpass.getpass("Escriba la contaseña: ")
			os.system("gpg1 --batch --passphrase %s %s/Datos_Usuarios.s3db.gpg" %(contrasena_principal, usuario_validar))
			f.close()

			#Si se abre bien la base de datos (si tiene permisos)
			try:
				#Para ver si la contraseña es correcta:
				con=sqlite3.connect("%s/Datos_Usuarios.s3db" %(usuario_validar)) #abrir la conexion
				cursor=con.cursor()

			except:
				print "Problema al abrir la Base de Datos."
				time.sleep(1)
				validar()


			try:
				cursor.execute("select * from Usuarios")
				for Usuarios in cursor:
					correcto=True   # Le anyado la variable para que no me vuelva aqui al salir.
					break #Para que se salga del bucle y no lea los datos

			except:
				print "Contrasena incorrecta."
				time.sleep(1)
				commands.getoutput("rm %s/Datos_Usuarios.s3db" %(usuario_validar))
				validar()

			if correcto==True:
				#Cierro conexion con Base de Datos.
				con.close()

				#Creo un fichero txt con la hora y el dia de inicio de sesion.
				# --- Comando tee -a añade el comando seleccionado al archivo txt sin borrar el contenido. ---
				commands.getoutput("date | tee -a %s/Ultimo_Inicio.txt" %(usuario_validar))

				#Compruebo si existe el fichero correo.txt
				correo=commands.getoutput("ls %s/correo.txt" %(usuario_validar))

				#Si existe correo.txt:
				if correo=="%s/correo.txt" %(usuario_validar):
					#Envio un correo del inicio de sesion con la fecha y el usuario.
					print "Cargando..."
					f=open("%s/correo.txt" %(usuario_validar))
					direccion_correo=f.next()
					os.system("echo Se ha iniciado sesion en el programa. Fecha: $(date). Usuario: %s | mail -s Usuarios.py %s" %(usuario_validar, direccion_correo))
					menu(usuario_validar, contrasena_principal)

				#Si no existe correo.txt
				else:
					menu(usuario_validar, contrasena_principal)

		#Si el usuario no es correcto
		else:
			print "Usuario incorrecto."

                        #La linea de abajo es para que se guarde el usuario con la fecha en el archivo Error_user.txt
                        commands.getoutput("echo Usuario: %s / Fecha: $(date) | tee -a Error_user.txt" %(usuario_validar))
			time.sleep(1)
			f.close()
			validar()




def menu(usuario_validar, contrasena_principal):
	try:
		os.system("clear")
		fecha=time.strftime("%x")
		hora=time.strftime("%X")
		print """
		Fecha: %s  Usuario: %s
                Hora: %s   Version: 3.0
		---------------------------------------
				MENU PRINCIPAL
		---------------------------------------


				1. Agregar datos
				2. Modificar datos
				3. Eliminar datos
				4. Ver datos
				5. Hacer consulta
				6. Salir

		""" %(fecha,usuario_validar, hora)
		orden=raw_input("Selecciona la opcion que deseas: ")


		def agregar():
			con=sqlite3.connect("%s/Datos_Usuarios.s3db" %(usuario_validar)) #abrir la conexion
			os.system("clear")

			print """
		                    Menu Agregar
		                *********************
		                        """





		  	lugar=raw_input("Escribe el lugar: ")
		  	usuario=raw_input("Escribe el usuario: ")
		  	contrasena=raw_input("Escribe la contrasena: ")

		  	print ""
		  	print "Lugar: %s" %(lugar)
		  	print "Usuario: %s"%(usuario)
		  	print "Contrasena: %s"%(contrasena)
		  	print ""

		  	seguro=raw_input("Deseas agregar esta informacion? (S/N): ")

		  	if seguro=="S" or seguro=="s":

		  		try:
			  		os.system("clear")
					cursor=con.cursor()
					print "Agregando..."
					cursor.execute("insert into Usuarios (Lugar, Usuario, Contrasena) values (?, ?, ?)", (lugar, usuario, contrasena))
					con.commit()
					con.close() #cerrar la conexion
					print "Informacion agregada con exito!"
					time.sleep(2)
					os.system("clear")
					menu(usuario_validar, contrasena_principal)


				except sqlite3.IntegrityError:
					con.close() #para cerrar la conexion porque sino da error en la base de datos.
					os.system("clear")
					print "Ya existen datos de %s." %(lugar)
					time.sleep(2)
					agregar()


				except sqlite3.ProgrammingError:
					con.close()
					os.system("clear")
					print "\nERROR"
					print "%s contiene un caracter extraño y no se puede agragar." %(lugar)
					raw_input("\nPulse una tecla para volver al menu Agregar...")
					os.system("clear")
					agregar()

		  	elif seguro=="N" or seguro=="n":
		  		os.system("clear")
		  		print "Informacion no agregada."
		  		time.sleep(2)
		  		os.system("clear")
		  		menu(usuario_validar, contrasena_principal)

		  	else:
		  		os.system("clear")
		  		print "Informacion no agregada."
		  		time.sleep(2)
		  		os.system("clear")
		  		menu(usuario_validar, contrasena_principal)



		def modificar():
			con=sqlite3.connect("%s/Datos_Usuarios.s3db" %(usuario_validar)) #abrir la conexion
			cursor=con.cursor()
			os.system("clear")
			print """
		                        Menu Modificar
		                     *********************
		        """





			cambio_lugar=raw_input("Escribe el nombre de la plataforma para modificar la informacion: ")

			if cambio_lugar=="s" or cambio_lugar=="S":
				menu(usuario_validar, contrasena_principal)

			sql=("select * from Usuarios where Lugar='%s'")%(cambio_lugar)
			cursor.execute(sql)

			Lugar=False

			for Usuarios in cursor:

			  	print ""
				print "1. Lugar: %s" %(Usuarios[0])
				print "2. Usuario: %s"%(Usuarios[1])
				print "3. Contrasena: %s"%(Usuarios[2])
				print ""

			  	Lugar=True

			if Lugar==False:
				print "No hay datos con ese nombre de Lugar registrados."
				time.sleep(2)
				os.system("clear")
				modificar()


			variable=raw_input("Escribe el numero de la variable a modificar: ")

			if variable=="1":
				lugar=raw_input("Escribe el nuevo nombre del lugar: ")
				sql="Update Usuarios set Lugar='%s' where Lugar='%s'" %(lugar, cambio_lugar)
				os.system("clear")
				print "Cargando..."
				cursor.execute(sql)
				con.commit()
				con.close()
				print "El nombre de la plataforma ha sido modificado con exito!"
				time.sleep(1.5)
				os.system("clear")
				menu(usuario_validar, contrasena_principal)

			elif variable=="2":
				usuario=raw_input("Escribe el nuevo nombre de usuario: ")
				sql="Update Usuarios set Usuario='%s' where Lugar='%s'" %(usuario, cambio_lugar)
				os.system("clear")
				print "Cargando..."
				cursor.execute(sql)
				con.commit()
				con.close()
				print "El nombre del usuario ha sido modificado con exito!"
				time.sleep(1.5)
				os.system("clear")
				menu(usuario_validar, contrasena_principal)

			elif variable=="3":
				contrasena=raw_input("Escriba la nueva contrasena: ")
				sql="Update Usuarios set Contrasena='%s' where Lugar='%s'" %(contrasena, cambio_lugar)
				os.system("clear")
				print "Cargando..."
				cursor.execute(sql)
				con.commit()
				con.close()
				print "La contrasena ha sido modificada con exito!"
				time.sleep(1.5)
				os.system("clear")
				menu(usuario_validar, contrasena_principal)

			elif variable=="s":
				menu(usuario_validar, contrasena_principal)

			else:
				print "Selecciona una opcion correcta.\nVariable del 1 al 3."
				time.sleep(1.5)
				os.system("clear")
				modificar()




		def eliminar():
			con=sqlite3.connect("%s/Datos_Usuarios.s3db" %(usuario_validar)) #abrir la conexion
			cursor=con.cursor()
			os.system("clear")
			print """

					Menu Eliminar
				 	***************
					"""

			eliminar_lugar=raw_input("Esciba el nombre del lugar que desea eliminar: ")
			if eliminar_lugar=="s" or eliminar=="S":
				menu(usuario_validar, contrasena_principal)

			sql=("select * from Usuarios where Lugar='%s'")%(eliminar_lugar)
			cursor.execute(sql)
			Lugar=False
			for Usuarios in cursor:

			  	print ""
				print "1. Lugar: %s" %(Usuarios[0])
				print "2. Usuario: %s"%(Usuarios[1])
				print "3. Contrasena: %s"%(Usuarios[2])
				print ""

			  	Lugar=True

			if Lugar==False:
			  	print "No hay datos con ese nombre de Lugar registrados."
				time.sleep(2)
				os.system("clear")
				eliminar()

			seguro=raw_input("Esta seguro que desea eliminar los datos de %s? (S/N): " %(eliminar_lugar))

			if seguro=="S" or seguro=="s":
				os.system("clear")
				print "Cargando..."
				sql="delete from Usuarios where Lugar='%s'" %(eliminar_lugar)
				cursor.execute(sql)
				con.commit()
				con.close()
				print "Los datos de %s han sido eliminados." %(eliminar_lugar)
				time.sleep(1.5)
				os.system("clear")
				menu(usuario_validar, contrasena_principal)

			elif seguro=="N" or seguro=="n":
				os.system("clear")
				print "Datos de %s no eliminados." %(eliminar_lugar)
				time.sleep(1.5)
				os.system("clear")
				menu(usuario_validar, contrasena_principal)


			else:
				os.system("clear")
				print "Datos de %s no eliminados." %(eliminar_lugar)
				time.sleep(1.5)
				os.system("clear")
				menu(usuario_validar, contrasena_principal)



		def ver():
			con=sqlite3.connect("%s/Datos_Usuarios.s3db" %(usuario_validar)) #abrir la conexion
			cursor=con.cursor()
			os.system("clear")
			print """

				Menu Ver
				****************
				"""

			cursor.execute("select * from Usuarios")

			for Usuarios in cursor:

				print ""
				print "______________________________________"
				print "|"
				print "| 1. Lugar: %s" %(Usuarios[0])
				print "| 2. Usuario: %s"%(Usuarios[1])
				print "| 3. Contrasena: %s"%(Usuarios[2])
				print "|_____________________________________"
				print ""
				print ""


			raw_input("Pulse una tecla para volver al menu...")
			con.commit()
			con.close()
			os.system("clear")
			menu(usuario_validar, contrasena_principal)




		def consulta():
			con=sqlite3.connect("%s/Datos_Usuarios.s3db" %(usuario_validar)) #abrir la conexion
			cursor=con.cursor()
			os.system("clear")
			print """

				Menu Consulta
			*******************

			Recuerda poner * para consultar un caracter.

			"""


			def consulta_noexacta():
				sql="select * from Usuarios where Lugar LIKE '%%%s%%'" %(consulta_lugar)
				cursor.execute(sql)

				existe=False
				os.system("clear")
				for Usuarios in cursor:
					print ""
					print "______________________________________"
					print "|"
					print "| 1. Lugar: %s" %(Usuarios[0])
					print "| 2. Usuario: %s"%(Usuarios[1])
					print "| 3. Contrasena: %s"%(Usuarios[2])
					print "|_____________________________________"
					print ""
					print ""
					salir=raw_input("Pulsa 'enter' para ver el siguiente registro o 's' para salir...")
					os.system("clear")

					if salir=="s":
						con.commit()
						con.close()

						consulta()
					existe=True

				if existe==False:
					print "No existen datos de ese lugar con ese nombre."
					time.sleep(1.5)
					consulta()
					con.commit()
					con.close()

				print "No hay mas registros!"
				time.sleep(1)
				con.commit()
				con.close()
				os.system("clear")
				menu(usuario_validar, contrasena_principal)





			def consulta_exacta():
				sql="select * from Usuarios where Lugar='%s'" %(consulta_lugar)
				cursor.execute(sql)
				existe=False

				for Usuarios in cursor:
					print ""
					print "______________________________________"
					print "|"
					print "| 1. Lugar: %s" %(Usuarios[0])
					print "| 2. Usuario: %s"%(Usuarios[1])
					print "| 3. Contrasena: %s"%(Usuarios[2])
					print "|_____________________________________"
					print ""
					print ""
					existe=True

				if existe==False:
					print "No existen datos de ese lugar con ese nombre."
					time.sleep(1.5)
					consulta()
					con.commit()
					con.close()

				raw_input("Pulse una tecla para volver al menu...")
				con.commit()
				con.close()
				os.system("clear")
				menu(usuario_validar, contrasena_principal)





			consulta_lugar=raw_input("Escriba el nombre del lugar que desea consultar: ")
			if consulta_lugar=="s" or consulta_lugar=="S":
				os.system("clear")
				menu(usuario_validar, contrasena_principal)


			if "*" in consulta_lugar:
				#Para remplazar el asterisco por nada
				consulta_lugar=consulta_lugar.replace("*","")
				consulta_noexacta()


			else:
				consulta_exacta()


		def copiabd():
			import commands
			os.system("clear")

			#PARA DETECTAR SI EL USB ESTA CONECTADO:
			usb=commands.getoutput("ls /media/copia")
			if usb=="":
				print "USB no detectado."
				time.sleep(2)
				menu(usuario_validar, contrasena_principal)

			else:
				#COPIO LA BASE DE DATOS DE /HOME/JUANMI ,PORQUE ES DONDE ESTA LA COPIA DE SEGURIDAD, AL USB
				commands.getoutput("cp /home/juanmi/Datos_Usuarios.s3db.gpg /media/copia/JUANMITRABA/Todo/Programacion_Ubuntu_python/")
				print "Bd copiada de /home/juanmi/ a USB con cifrado gpg."
				commands.getoutput("cp Usuarios/Usuarios.py /media/copia/JUANMITRABA/Todo/Programacion_Ubuntu_python/")
				print "Script de 'Usuarios.py' copiado en USB."
				time.sleep(2)
				menu(usuario_validar, contrasena_principal)


		def cambio_usuario(usuario_validar):
			os.system("clear")
			#LO ABRO EN MODO LECTURA
			f=open("Users.txt", "r")

			#LEO LAS LINEAS PARA DESPUES PODER ESCRIBIRLAS EN EL CAMBIO DE USUARIO
			l1=f.next()
			usuario0=f.next()
			usuario1=f.next()
			usuario2=f.next()

			#LE QUITO LA -
			usuario0=usuario0.replace("-","")
			usuario1=usuario1.replace("-","")
			usuario2=usuario2.replace("-","")

			#LE QUITO EL SALTO DE LINEA (\n) PARA QUE NO ME DE ERROR DE QUE NO ES IGUAL AL usuario_validar
			usuario0=usuario0.replace("\n","")
			usuario1=usuario1.replace("\n","")
			usuario2=usuario2.replace("\n","")

			#MUESTRO LOS USUARIOS
			print "USUARIOS:"
			print ""
			print "1. %s" %(usuario0)
			print "2. %s" %(usuario1)
			print "3. %s" %(usuario2)
			print ""

			numero=raw_input("Escribe el numero del usuario a modificar: ")

			if numero=="1":
				#ABRO EN MODO ESCRITURA EL TXT
				f2=open("Users.txt", "w")
				new_user=raw_input("Escribe el nuevo nombre de usuario a reemplazar por %s: " %(usuario0))

				#HAGO QUE ESCRIBA TODOS LOS DATOS CAMBIANDO SOLO EL USUARIO. EL \n ES PARA QUE SALTE LA LINEA
				f2.write("%s%s\n%s\n%s\n" %(l1,new_user, usuario1, usuario2))
				os.system("clear")

				#Cambio el nombre de la carpeta:
				os.system("mv %s %s" %(usuario0, new_user))

				print "El usuario ha sido actualizado!"
				f2.close()

				#Si se modifica el usuario con el que has entrado
				if usuario_validar==usuario0:
					#Actualizo el usuario_validar que ha cambiado a new_user
					usuario_validar=new_user

				time.sleep(1.5)

			elif numero=="2":
				f2=open("Users.txt", "w")
				new_user=raw_input("Escribe el nuevo nombre de usuario a reemplazar por %s: " %(usuario1))
				f2.write("%s%s\n%s\n%s\n" %(l1,usuario0, new_user, usuario2))
				os.system("clear")
				os.system("mv %s %s" %(usuario1, new_user))
				print "El usuario ha sido actualizado!"
				f2.close()
				if usuario_validar==usuario1:
					usuario_validar=new_user
				time.sleep(1.5)

			elif numero=="3":
				f2=open("Users.txt", "w")
				new_user=raw_input("Escribe el nuevo nombre de usuario a reemplazar por %s: " %(usuario2))
				f2.write("%s%s\n%s\n%s\n" %(l1,usuario0, usuario1, new_user))
				os.system("clear")
				os.system("mv %s %s" %(usuario2, new_user))
				print "El usuario ha sido actualizado!"
				f2.close()
				if usuario_validar==usuario1:
					usuario_validar=new_user
				time.sleep(1.5)

			else:
				print "Escoge una opcion correcta!"
				time.sleep(1.5)
				f.close()
				cambio_usuario(usuario_validar)

			#CIERRO LA CONEXION CON F
			f.close()
			menu(usuario_validar, contrasena_principal)


		def restaurar():
			import commands
			os.system("clear")
			print "Has seleccionado la opcion de restaurar la Base de datos"
			print ""
			seguro=raw_input("Estas seguro que deseas eliminar todos los datos? (S/N) ")

			if seguro=="s" or seguro=="S":
				commands.getoutput("rm %s/Datos_Usuarios.s3db" %(usuario_validar))
				commands.getoutput("mv Datos_Usuarios_vacio.s3db %s/Datos_Usuarios.s3db" %(usuario_validar))
				commands.getoutput("cp %s/Datos_Usuarios.s3db Datos_Usuarios_vacio.s3db" %(usuario_validar))
				print "Base de datos restaurada con exito."
				time.sleep(1)
				menu(usuario_validar, contrasena_principal)

			else:
				print "Base de datos no eliminada."
				time.sleep(1.5)
				menu(usuario_validar, contrasena_principal)


		def contar():
			os.system("clear")
			con=sqlite3.connect("%s/Datos_Usuarios.s3db" %(usuario_validar)) #abrir la conexion
                        cursor=con.cursor()
                        cursor.execute("select * from Usuarios")

			count=0
			for Usuarios in cursor:
				count=count+1


			con.commit()
			con.close()
			print ""
			print "Hay un total de %s registros" %(count)
			print ""
			raw_input("Presiona una tecla para volver al menu...")
			menu(usuario_validar, contrasena_principal)

		def error_user():
			os.system("clear")
			os.system("cat Error_user.txt")
			print ""
			raw_input("Pulsa 'enter' para volver al menu...")
			menu(usuario_validar, contrasena_principal)


		def ultimo_inicio():
			os.system("clear")
			os.system("cat %s/Ultimo_Inicio.txt" %(usuario_validar))
			print ""
			raw_input("Pulsa 'enter' para volver al menu...")
			menu(usuario_validar, contrasena_principal)


		def correo(usuario_validar):
			import commands
			os.system("clear")
			print """
				******************
					MENU CORREO
				******************
			"""

			#---- FALTA COMPROBACION DE SI EL CORREO ESTA INSTALADO O NO ----

			#Para comprobar si existe un archivo con un correo.
			touch_correo=commands.getoutput("ls %s/correo.txt" %(usuario_validar))

			#Si existe el archivo correo.txt
			if touch_correo=="%s/correo.txt" %(usuario_validar):

				#Lo abro en modo lectura.
				f=open("%s/correo.txt" %(usuario_validar))

				#Leo la cuenta de correo
				cuenta_correo=f.next()

				print "Esta opción actualmente está habilitada.\nSe enviará el mensaje al correo '%s' cada vez que se inicie sesion." %(cuenta_correo)
				print ""
				modificar_correo=raw_input("Deseas modificar el correo? (S/N): ")

				#Cierro el modo lectura
				f.close()

				if modificar_correo=="S" or modificar_correo=="s":
					print ""
					nueva_cuenta_correo=raw_input("Introduce la nueva cuenta de correo: ")

					#Lo abro en modo escritura
					f=open("%s/correo.txt" %(usuario_validar) ,"w")

					#Remplazo la cuenta de correo por la nueva
					f.write(nueva_cuenta_correo)

					#Cierro conexion y me voy al menu
					f.close()
					print "Correo '%s' guardado con exito." %(nueva_cuenta_correo)
					time.sleep(2)
					menu(usuario_validar, contrasena_principal)


				else:
					print "Volviendo al menu..."
					f.close()
					time.sleep(1.5)
					menu(usuario_validar, contrasena_principal)


			#Si no existe el archivo correo.txt
			else:
				print "Con esta opción habilitada, se te enviará un correo electronico a la cuenta que indiques, cada vez que se inicie sesion con tu usuario."
				print ""
				correo=raw_input("Deseas habilitar esta opción? (S/N): ")

				if correo=="s" or correo=="S":
					#Creo el txt del correo
					commands.getoutput("touch %s/correo.txt" %(usuario_validar))

					#Lo abro en modo escritura
					f=open("%s/correo.txt" %(usuario_validar), "w" )
					correo_deseas=raw_input("Escribe la direccion de correo a la que se enviara el mensaje: ")

					#Escribo en el archivo la direccion de correo
					f.write(correo_deseas)

					print ""
					print "Cuenta de correo '%s' guardada con exito." %(correo_deseas)
					f.close()
					time.sleep(2)
					menu(usuario_validar, contrasena_principal)

				else:
					print "Opcion no habilitada."
					time.sleep(1.5)
					menu(usuario_validar, contrasena_principal)


		def help():
			os.system("clear")
			print """
		Opciones ocultas:
		(Escribirlas en el menu)

		· copia = Para copiar Base de datos y Script en USB.
		· cambio.user = Para cambiar los nombre de los usuarios.
		· restore = Para eliminar todos los datos de la Base de datos.
		· count = Para contar el total de los registros de la Base de datos.
		· error.user = Para saber los intentos fallidos de inicio de sesion.
		· ultimo.inicio = Para saber la fecha y la hora de los ultimos inicio de sesion.
		· correo = Para habilitar un mensaje por correo cada vez que se inicia sesion.
		"""

			raw_input("Pulsa cualquier tecla para volver al menu...")
			menu(usuario_validar, contrasena_principal)

		#CONTINUACION DE MENU. (LO PONGO AQUI PARA QUE NO ME DE ERROR.)

		if orden=="1":
			agregar()

		elif orden=="2":
			modificar()

		elif orden=="3":
			eliminar()

		elif orden=="4":
			ver()

		elif orden=="5":
			consulta()

		elif orden=="6":
			import commands
			commands.getoutput("rm %s/Datos_Usuarios.s3db.gpg" %(usuario_validar))
			os.system("gpg1 -c --batch --passphrase %s %s/Datos_Usuarios.s3db" %(contrasena_principal, usuario_validar))
			correcto=commands.getoutput("ls %s/Datos_Usuarios.s3db.gpg" %(usuario_validar))

			#PARA SABER SI REPITO BIEN LA CONTRASENA
			if correcto=="%s/Datos_Usuarios.s3db.gpg" %(usuario_validar):
				commands.getoutput("rm %s/Datos_Usuarios.s3db" %(usuario_validar))
				commands.getoutput("rm %s/usuarios.pdf"%(usuario_validar))
				os.system("cp -r %s /media/copia/Usuarios" %(usuario_validar))
                                #os.system("clear")
				exit()

			else:
				print "Las contrasenas no coinciden!!"
				time.sleep(2)
				menu(usuario_validar, contrasena_principal)

		elif orden=="copia":
			copiabd()

		elif orden=="cambio.user":
			cambio_usuario(usuario_validar)

		elif orden=="restore":
			restaurar()

		elif orden=="count":
			contar()

		elif orden=="help":
			help()

		elif orden=="error.user":
			error_user()

		elif orden=="ultimo.inicio":
			ultimo_inicio()

		elif orden=="correo":
			correo(usuario_validar)

		else:
			print "Introduzca el numero de la opcion correcta."
			time.sleep(1)
			os.system("clear")
			menu(usuario_validar, contrasena_principal)

	except KeyboardInterrupt:
		menu(usuario_validar, contrasena_principal)










import os, time, sqlite3, sys, getpass, commands
validar()
